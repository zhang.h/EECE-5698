Course: Robotics Sensing and Navigation

Language: Python, C++

Description:

ROS Development 

Lab 1:

Receive GPS NMEA data via USB-Serial port

Extract and send information (Time, Location...) using LCM libraries

Reference: https://lcm-proj.github.io/

MATLAB scripts included (Data Analysis)

Lab 2:

Receive data from RTK GPS using TCP socket

Compare it with the normal GPS receiver

MATLAB scripts for data visualization included

Lab 3: 
Introduction to Inertial Navigation Systems

Collect both IMU and GPS data on a moving car

Plot trajectories based on IMU or GPS data using MATLAB

Lab 4:

Camera calibration and image mosaicing

Final Project:

Sensor Fusion on SLAM

Platform: Turtlebot 3

Reference: http://emanual.robotis.com/docs/en/platform/turtlebot3/overview/

See the readme file for details