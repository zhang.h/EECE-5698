%Load LCM Class
javaaddpath gps_lcm.jar
javaaddpath lcm.jar
%Load LCM log file
log_file = lcm.logging.Log('log-1630.log', 'r');
%Read log file and interpret into array
count = 1;
utm_x = double(zeros(1, 1000));
utm_y = double(zeros(1, 1000));
lat = double(zeros(1, 1000));
long = double(zeros(1, 1000));
while true
    try
        ev = log_file.readNext();
        
        if strcmp(ev.channel, 'GPSData')
            gpsinfo = gpslcm.gps_lcm(ev.data);
            utm_x(count) = gpsinfo.utm_x;
            utm_y(count) = gpsinfo.utm_y;
            lat(count) = gpsinfo.latitude;
            long(count) = gpsinfo.longitude;
        end
        
        count = count + 1;
        
    catch err
        break;
    end
end
%remove blank data
% utm_x = utm_x(1:count-1);
% utm_y = utm_y(1:count-1);
lat = lat(1:count-1);
long = long(1:count-1);
position = geoshape(lat(70:130),long(70:130));
utm_x = utm_x(70:130);
utm_y = utm_y(70:130);
utm_x = utm_x - min(utm_x);
utm_y = utm_y - min(utm_y);

figure
scatter(utm_x, utm_y, 'filled');
xlabel('UTM X (m)')
ylabel('UTM Y (m)')
hold on
p = polyfit(utm_x, utm_y, 1);
utm_y_fit = polyval(p, utm_x);
plot(utm_x(5:end), utm_y_fit(5:end), 'k--', 'LineWidth', 2);
hold off
webmap('openstreetmap')
wmline(position)
