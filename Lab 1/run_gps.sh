#!/bin/bash

python readGPS_lcm.py /dev/ttyACM0 &

export CLASSPATH=gps_lcm.jar
lcm-logger -s log1.log &
lcm-spy &

wait %3

kill %1 %2
