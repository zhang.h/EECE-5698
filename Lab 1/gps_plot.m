%Load LCM Class
javaaddpath gps_lcm.jar
javaaddpath lcm.jar
%Load LCM log file
log_file = lcm.logging.Log('log-1635.log', 'r');
%Read log file and interpret into array
count = 1;
utm_x = double(zeros(1, 1000));
utm_y = double(zeros(1, 1000));
while true
    try
        ev = log_file.readNext();
        
        if strcmp(ev.channel, 'GPSData')
            gpsinfo = gpslcm.gps_lcm(ev.data);
            utm_x(count) = gpsinfo.utm_x;
            utm_y(count) = gpsinfo.utm_y;
        end
        
        count = count + 1;
        
    catch err
        break;
    end
end
%remove blank data
utm_x = utm_x(1:count-1);
utm_y = utm_y(1:count-1);
utm_x = utm_x - min(utm_x);
utm_y = utm_y - min(utm_y);
figure
scatter(utm_x, utm_y, 5, 'filled');