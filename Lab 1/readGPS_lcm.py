# encoding: utf-8
# python 3
# based on NMEA protocol

# for python 2 compatibility
from __future__ import print_function
import sys
import serial
import lcm
from gpslcm import gps_lcm
from class_gps import rawPos

class gpsData:
    def __init__(self, portName):
        # serial port initialization
        print('GPS Device on Serial Port {}...'.format(portName))
        try:
            self.port = serial.Serial(portName, 4800, timeout=1.0)
        except:
            print('Serial port couldn\'t open! Exit.')
            sys.exit()

        # initialization finished when NMEA message received
        print('Serial port initializing...')
        errorCount = 0
        while True:
            serData = self.port.readline().decode()
            if serData.startswith('$GPGGA,'):
                print('Initialization finished!')
                break
            if errorCount > 20:
                print('Initialization failed! Exit!')
                sys.exit()
            else:
                errorCount += 1

    def showPos(self):
        # print GPS data
        print('Make sure strong GPS signal!')
        while True:
            serData = self.port.readline().decode()
            if serData.startswith('$GPGGA,'):
                posData = serData.split(',')
                if int(posData[6]) == 0:
                    print('Weak GPS signal!')
                    continue
                else:
                    rawData = rawPos(serData)
                    rawData.printPos()
                    break

    # generate the LCM message
    def packMsg(self, rawPos_class):
        gpsMsg = gps_lcm()
        gpsMsg.time = rawPos_class.time
        gpsMsg.latitude = rawPos_class.latitude
        gpsMsg.ns = rawPos_class.NS
        gpsMsg.longitude = rawPos_class.longitude
        gpsMsg.sw = rawPos_class.SW
        gpsMsg.altitude = rawPos_class.altitude
        gpsMsg.hour = rawPos_class.hour
        gpsMsg.minute = rawPos_class.minute
        gpsMsg.second = rawPos_class.second
        gpsMsg.lat_deg = rawPos_class.lat_deg
        gpsMsg.lat_cent = rawPos_class.lat_cent
        gpsMsg.lat_sec = rawPos_class.lat_sec
        gpsMsg.long_deg = rawPos_class.long_deg
        gpsMsg.long_cent = rawPos_class.long_cent
        gpsMsg.long_sec = rawPos_class.long_sec
        gpsMsg.utm_x = rawPos_class.utm_x
        gpsMsg.utm_y = rawPos_class.utm_y
        gpsMsg.utm_zonenumber = rawPos_class.utm_zonenumber
        gpsMsg.utm_zoneletter = rawPos_class.utm_zoneletter
        gpsMsg.gpgga = rawPos_class.original_str
        return gpsMsg

    def getPos(self,count):
        # read NMEA message and send it using LCM
        # use "count" to set the number of the sent message, 0 for infinity
        if count == 0:
            nonstopFlag = True
        else:
            nonstopFlag = False
        print('Make sure strong GPS signal!')
        gpsLCM = lcm.LCM()
        while True:
            if count == 0 and nonstopFlag == False:
                break
            serData = self.port.readline().decode()
            if serData.startswith('$GPGGA,'):
                posData = serData.split(',')
                if int(posData[6]) == 0:
                    print('Weak GPS signal!')
                    continue
                else:
                    sentMsg = self.packMsg(rawPos(serData))
                    gpsLCM.publish('GPSData', sentMsg.encode())
                    print('GPS data sent!')
                    count -= 1
                    continue

    def __exit__(self):
        self.port.close()

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('Usage: {} <serial_port>'.format(sys.argv[0]))
        sys.exit(0)
    myGPS = gpsData(sys.argv[1])
    myGPS.showPos()
    myGPS.getPos(0)
