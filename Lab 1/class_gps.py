# encoding: utf-8
# python 3
# based on NMEA protocol

# for python 2 compatibility
from __future__ import print_function
import utm

class rawPos:
    # read $GPGGA string and extract gps info
    def __init__(self, gps_serial_data):
        gpgga = gps_serial_data.split(',')
        self.original_str = gps_serial_data
        self.time = int(float(gpgga[1]))
        self.latitude = float(gpgga[2])/100
        self.NS = gpgga[3]
        self.longitude = float(gpgga[4])/100
        self.SW = gpgga[5]
        self.altitude = int(float(gpgga[9])) # unit: meter

        self.hour = self.time//10000
        self.minute = (self.time - self.hour*10000)//100
        self.second = self.time - self.hour*10000 - self.minute*100
        self.lat_deg = int(self.latitude)
        self.lat_cent = int(float(gpgga[2]) - self.lat_deg*100)
        self.lat_sec = int((float(gpgga[2]) - self.lat_deg*100 - self.lat_cent)*60)
        self.long_deg = int(self.longitude)
        self.long_cent = int(float(gpgga[4]) - self.long_deg*100)
        self.long_sec = int((float(gpgga[4]) - self.long_deg*100 - self.long_cent)*60)

        # compute the latitude and longitude
        self.latitude = self.lat_deg + (float(gpgga[2])-self.lat_deg*100)/60.0
        if self.NS == 'S':
            self.latitude = -self.latitude
        self.longitude = self.long_deg + (float(gpgga[4])-self.long_deg*100)/60.0
        if self.SW == 'W':
            self.longitude = -self.longitude

        # convert to UTM coordinates
        utm_temp = utm.from_latlon(self.latitude, self.longitude)
        self.utm_x = utm_temp[0]
        self.utm_y = utm_temp[1]
        self.utm_zonenumber = utm_temp[2]
        self.utm_zoneletter = utm_temp[3]

    # print GPS data
    def printPos(self):
        print('Latitude:',end=' ')
        print('{0} degrees {1}\' {2}\" {3}'.format(self.lat_deg,self.lat_cent,self.lat_sec,self.NS))
        print('Longitude:',end=' ')
        print('{0} degrees {1}\' {2}\" {3}'.format(self.long_deg,self.long_cent,self.long_sec,self.SW))
        print('Altitude: {} m'.format(self.altitude))
        print('GPS Time: {0}:{1}:{2} UTC'.format(self.hour,self.minute,self.second))
