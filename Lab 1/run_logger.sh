#!/bin/bash

export CLASSPATH=gps_lcm.jar
lcm-logger -s log-%H%M%S.log &
lcm-spy &

wait %2

kill %1
