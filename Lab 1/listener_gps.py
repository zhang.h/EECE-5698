# encoding: utf-8
# python 3
# based on NMEA protocol

# for python 2 compatibility
from __future__ import print_function
import sys
import lcm
from gpslcm import gps_lcm
from class_gps import rawPos

def gpslcm_handler(channel, data):
    msg = gps_lcm.decode(data)
    gps_data = rawPos(msg.gpgga)
    gps_data.printPos()

if __name__ == "__main__":
    recv_lcm = lcm.LCM()
    subscription = recv_lcm.subscribe('GPSData', gpslcm_handler)
    while True:
        try:
            recv_lcm.handle()
        except KeyboardInterrupt:
            break
    recv_lcm.unsubscribe(subscription)
