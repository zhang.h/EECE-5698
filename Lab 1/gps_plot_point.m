%Load LCM Class
javaaddpath gps_lcm.jar
javaaddpath lcm.jar
%Load LCM log file
log_file = lcm.logging.Log('log-1635.log', 'r');
%Read log file and interpret into array
count = 1;
utm_x = double(zeros(1, 1000));
utm_y = double(zeros(1, 1000));
lat = double(zeros(1, 1000));
long = double(zeros(1, 1000));
while true
    try
        ev = log_file.readNext();
        
        if strcmp(ev.channel, 'GPSData')
            gpsinfo = gpslcm.gps_lcm(ev.data);
            utm_x(count) = gpsinfo.utm_x;
            utm_y(count) = gpsinfo.utm_y;
            lat(count) = gpsinfo.latitude;
            long(count) = gpsinfo.longitude;
        end
        
        count = count + 1;
        
    catch err
        break;
    end
end
%remove blank data
lat = lat(1:count-1);
long = long(1:count-1);
position = geoshape(lat(70:130),long(70:130));
utm_x = utm_x(1:count-1);
utm_y = utm_y(1:count-1);
utm_x_min = min(utm_x);
utm_y_min = min(utm_y);
utm_x = utm_x - utm_x_min;
utm_y = utm_y - utm_y_min;

figure
x_hist=histogram(utm_x,int32(max(utm_x)-min(utm_x)));
xlabel('UTM X (m)')
ylabel('Counts')
[~,x_max] = max(x_hist.BinCounts);
figure
histfit(utm_x)
xlabel('UTM X (m)')
ylabel('Counts')
figure
y_hist=histogram(utm_y,int32(max(utm_y)-min(utm_y)));
xlabel('UTM Y (m)')
ylabel('Counts')
[~,y_max] = max(y_hist.BinCounts);
figure
histfit(utm_y)
xlabel('UTM Y (m)')
ylabel('Counts')


figure
scatter(utm_x, utm_y, 5, 'filled');
xlabel('UTM X (m)')
ylabel('UTM Y (m)')
hold on
temp = cat(1,utm_x, utm_y);
temp = transpose(temp);
[~, central, sumd]=kmeans(temp,1);
% plot(central(1), central(2), 'kx', 'LineWidth', 3, 'MarkerSize', 15);
plot(x_max, y_max, 'rx', 'LineWidth', 3, 'MarkerSize', 15);
hold off
format longG
central_x = central(1) + utm_x_min
central_y = central(2) + utm_y_min
estimate_x = x_max + utm_x_min
estimate_y = y_max + utm_y_min

webmap('openstreetmap')
wmline(position)