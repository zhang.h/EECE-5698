%Test custom corner detector
%% Load images
leftFile = 'wall1_s_1.jpg';
rightFile = 'wall1_s_2.jpg';
imgL = imread(leftFile);
imgR = imread(rightFile);
imgL = imrotate(imgL, 270);
imgR = imrotate(imgR, 270);
imL = rgb2gray(imgL);
imR = rgb2gray(imgR);
%% Detect corners
[yl, xl, ml] = harris(imL, 'tile', [5 5], 'disp');
[yr, xr, mr] = harris(imR, 'tile', [5 5], 'disp');
%% Extract features
[fl, pl] = extractFeatures(imL, [xl, yl]);
[fr, pr] = extractFeatures(imR, [xr, yr]);
%% Match features
pointPairs = matchFeatures(fl, fr, 'Unique', true);
matchedPointsL = pl(pointPairs(:,1),:);
matchedPointsR = pr(pointPairs(:,2),:);
figure
showMatchedFeatures(imgL, imgR, matchedPointsL, matchedPointsR, 'montage');
%% Estimate transform
[tform,inlierPointsR,inlierPointsL] = estimateGeometricTransform(matchedPointsR, matchedPointsL, 'projective');
figure
showMatchedFeatures(imgL, imgR, inlierPointsL, inlierPointsR, 'montage');
%% Pre-process for final panorama
%Compute the output limits for each transform
%tform is relative to the left image
imageSize = size(imL);
[xlim(1,:), ylim(1,:)] = outputLimits(tform, [1 imageSize(2)], [1 imageSize(1)]);
%Calculate the size of panorama
xMin = min([1; xlim(:)]);
xMax = max([imageSize(2); xlim(:)]);
yMin = min([1; ylim(:)]);
yMax = max([imageSize(1); ylim(:)]);
%Initialize the blank panorama
width  = round(xMax - xMin);
height = round(yMax - yMin);
panorama = zeros([height width 3], 'like', imgL);
%% Create the panorama
%Transformation for coordinate systems
xLimits = [xMin xMax];
yLimits = [yMin yMax];
panoramaView = imref2d([height width], xLimits, yLimits);
%Initialize the blender with binary mask(for warpped images)
blender = vision.AlphaBlender('Operation', 'Binary mask', 'MaskSource', 'Input port');
%Place the first (center) images
mask = imwarp(true(imageSize(1),imageSize(2)), projective2d(eye(3)), 'OutputView', panoramaView);
warpedImage = imwarp(imgL, projective2d(eye(3)), 'OutputView', panoramaView);
panorama = step(blender, panorama, warpedImage, mask);
%Place the second image
%Generate a binary mask
mask = imwarp(true(imageSize(1),imageSize(2)), tform, 'OutputView', panoramaView);
%Transform Image 2 into the panorama
%Backward warping, bilinear interpolation
warpedImage = imwarp(imgR, tform, 'OutputView', panoramaView);
%Overlay the warpedImage onto the panorama
panorama = step(blender, panorama, warpedImage, mask);
%% Show the result
figure
imshow(panorama)
%% Display corners
figure
imshow(imgL)
hold on
scatter(xl, yl, 'g+')
figure
imshow(imgR)
hold on
scatter(xr, yr, 'g+')