%Panorama using corner features
%The size of each image should be the same
%Partial codes are based on the official example of Panoramic Image Stitching by Mathworks
%% Load images
numFiles = 5;
img = cell(1,numFiles);
%Attention: the appropriate order required
for n = 1:numFiles
   currentFile = sprintf('wall1_s_%01d_rect_color.jpg', n);
   img{n} = imread(currentFile);
   img{n} = imrotate(img{n}, 270);
end
%Create intensity images
imgGray = cell(1, numFiles);
for n = 1:numFiles
   imgGray{n} = rgb2gray(img{n});
end
%% Find transformation matrices relative to the first image
tforms(numFiles) = projective2d(eye(3));
for n = 2:numFiles
    %Harris Corner Detection/Match Features by NSSD/Estimate the Homography by RANSAC
    %Gaussian filter size 5x5, sigma=5/3
    %Minimum accepted quality of corners 1%*MAX(R) (Threshold for non-maxima suppression)
    [y1, x1, m1] = harris(imgGray{n-1}, 1000, 'tile', [5 5]);
    [y2, x2, m2] = harris(imgGray{n}, 1000, 'tile', [5 5]);
    [features1, points1] = extractFeatures(imgGray{n-1}, [x1, y1]);
    [features2, points2] = extractFeatures(imgGray{n}, [x2, y2]);
    %Match features by NSSD
    nccPairs = matchFeatures(features2, features1, 'Unique', true);
    matchedPoints2 = points2(nccPairs(:,1),:);
    matchedPoints1 = points1(nccPairs(:,2),:);
%     figure
%     showMatchedFeatures(img{n-1}, img{n}, matchedPoints1, matchedPoints2, 'montage');
    %RANSAC: Confidence: 99(For 99%), Max Distance of inliers: 1.5
    tforms(n) = estimateGeometricTransform(matchedPoints2, matchedPoints1, 'projective', 'Confidence', 99.9, 'MaxNumTrials', 5000);
    %Compute T(n)xT(n-1)x...xT(1)
    tforms(n).T = tforms(n).T * tforms(n-1).T;
end
%% Pre-process for final panorama
%Compute the output limits for each transform
%Every matrix in tforms is relative to the first image
imageSize = size(imgGray{1});
xlim = zeros(numFiles,2);
ylim = zeros(numFiles,2);
for i = 1:numFiles
    [xlim(i,:), ~] = outputLimits(tforms(i), [1 imageSize(2)], [1 imageSize(1)]);
end
%Find center image
avgXLim = mean(xlim, 2);
[~, idx] = sort(avgXLim);
centerIdx = floor((numFiles+1)/2);
centerImageIdx = idx(centerIdx);
%Every matrix in tforms is relative to the center image
Tinv = invert(tforms(centerImageIdx));
for i = 1:numFiles
    tforms(i).T = tforms(i).T * Tinv.T;
end
%Calculate the size of panorama based on the minimum and maximum output limits
for i = 1:numFiles
    [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(2)], [1 imageSize(1)]);
end
xMin = min([1; xlim(:)]);
xMax = max([imageSize(2); xlim(:)]);
yMin = min([1; ylim(:)]);
yMax = max([imageSize(1); ylim(:)]);
%Initialize the blank panorama
width  = round(xMax - xMin);
height = round(yMax - yMin);
panorama = zeros([height width 3], 'like', img{1});
%% Create the panorama
%Transformation for coordinate systems
xLimits = [xMin xMax];
yLimits = [yMin yMax];
panoramaView = imref2d([height width], xLimits, yLimits);
%Initialize the blender using binary mask (for warpped images)
blender = vision.AlphaBlender('Operation', 'Binary mask', 'MaskSource', 'Input port');
%Place the image
for i = 1:numFiles
    %Generate a binary mask
    mask = imwarp(true(imageSize(1), imageSize(2)), tforms(i), 'OutputView', panoramaView);
    %Transform every image into the panorama
    %Backward warping, bilinear interpolation
    warpedImage = imwarp(img{i}, tforms(i), 'OutputView', panoramaView);
    %Overlay the warpedImage onto the panorama
    panorama = step(blender, panorama, warpedImage, mask);
end
%% Show the result
figure
imshow(panorama)
% imshow(insertShape(panorama, 'circle', [-xMin+imageSize(2)/2 -yMin+imageSize(1)/2 12], 'LineWidth', 10))