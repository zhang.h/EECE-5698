% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 500.992241013564580 ; 499.842995950344232 ];

%-- Principal point:
cc = [ 323.080701263515607 ; 240.121288179458304 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.142819029011271 ; -0.149400278154527 ; 0.000857342186805 ; 0.000074032694249 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 4.317281470087466 ; 5.601348781818476 ];

%-- Principal point uncertainty:
cc_error = [ 7.094472997006100 ; 2.698316273815557 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.030784101741719 ; 0.320547299014721 ; 0.002645986139511 ; 0.002934411026835 ; 0.000000000000000 ];

%-- Image size:
nx = 640;
ny = 480;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 22;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -2.634996e+00 ; -1.080744e+00 ; 6.672136e-01 ];
Tc_1  = [ -5.025529e+01 ; 2.737782e+01 ; 4.506158e+02 ];
omc_error_1 = [ 7.764946e-03 ; 3.039413e-03 ; 1.222619e-02 ];
Tc_error_1  = [ 6.215234e+00 ; 2.424511e+00 ; 5.209269e+00 ];

%-- Image #2:
omc_2 = [ -2.098194e+00 ; -1.586993e+00 ; 8.224013e-01 ];
Tc_2  = [ -3.492844e+01 ; -4.975987e+01 ; 4.309270e+02 ];
omc_error_2 = [ 8.920667e-03 ; 5.474511e-03 ; 1.132064e-02 ];
Tc_error_2  = [ 5.990519e+00 ; 2.324591e+00 ; 4.796250e+00 ];

%-- Image #3:
omc_3 = [ -1.407959e+00 ; -2.046218e+00 ; 5.479763e-01 ];
Tc_3  = [ 1.737238e+01 ; -9.947003e+01 ; 4.101068e+02 ];
omc_error_3 = [ 7.016597e-03 ; 8.498928e-03 ; 8.827997e-03 ];
Tc_error_3  = [ 5.872526e+00 ; 2.219853e+00 ; 4.623368e+00 ];

%-- Image #4:
omc_4 = [ -2.496882e+00 ; -1.023544e+00 ; 9.578917e-01 ];
Tc_4  = [ -5.636430e+01 ; 2.663940e+01 ; 4.585171e+02 ];
omc_error_4 = [ 9.462904e-03 ; 3.586227e-03 ; 1.300752e-02 ];
Tc_error_4  = [ 6.311650e+00 ; 2.474615e+00 ; 5.140513e+00 ];

%-- Image #5:
omc_5 = [ -2.389007e+00 ; -1.270552e+00 ; 8.247771e-01 ];
Tc_5  = [ -9.667877e+01 ; 1.990368e+01 ; 4.393753e+02 ];
omc_error_5 = [ 8.965457e-03 ; 4.221197e-03 ; 1.211035e-02 ];
Tc_error_5  = [ 5.912354e+00 ; 2.395727e+00 ; 5.005345e+00 ];

%-- Image #6:
omc_6 = [ -2.005255e+00 ; -2.040975e+00 ; 5.777307e-01 ];
Tc_6  = [ -9.860421e+01 ; -3.766429e+01 ; 4.551071e+02 ];
omc_error_6 = [ 6.938159e-03 ; 4.796037e-03 ; 1.162152e-02 ];
Tc_error_6  = [ 6.137033e+00 ; 2.486802e+00 ; 5.196285e+00 ];

%-- Image #7:
omc_7 = [ -2.309661e+00 ; -1.762245e+00 ; 4.186845e-01 ];
Tc_7  = [ -8.000560e+01 ; -1.285473e+01 ; 3.607393e+02 ];
omc_error_7 = [ 5.870137e-03 ; 3.867753e-03 ; 9.921890e-03 ];
Tc_error_7  = [ 4.850232e+00 ; 1.957389e+00 ; 4.157929e+00 ];

%-- Image #8:
omc_8 = [ -1.283000e+00 ; -2.489416e+00 ; 2.420006e-01 ];
Tc_8  = [ 3.525566e+01 ; -9.003377e+01 ; 3.871889e+02 ];
omc_error_8 = [ 4.322467e-03 ; 6.879191e-03 ; 9.583818e-03 ];
Tc_error_8  = [ 5.604519e+00 ; 2.107508e+00 ; 4.419063e+00 ];

%-- Image #9:
omc_9 = [ -2.352242e+00 ; -1.807230e+00 ; 3.841296e-01 ];
Tc_9  = [ -6.192859e+01 ; -5.309615e+01 ; 3.787973e+02 ];
omc_error_9 = [ 5.881018e-03 ; 3.816132e-03 ; 1.030638e-02 ];
Tc_error_9  = [ 5.171640e+00 ; 2.051353e+00 ; 4.301500e+00 ];

%-- Image #10:
omc_10 = [ -2.684746e+00 ; -1.491890e+00 ; 2.770624e-01 ];
Tc_10  = [ -6.907217e+01 ; -4.758767e+01 ; 3.977293e+02 ];
omc_error_10 = [ 7.527873e-03 ; 4.155329e-03 ; 1.139732e-02 ];
Tc_error_10  = [ 5.463511e+00 ; 2.150279e+00 ; 4.328421e+00 ];

%-- Image #11:
omc_11 = [ -1.519590e+00 ; -2.442767e+00 ; 2.267901e-01 ];
Tc_11  = [ -3.948068e+01 ; -8.854561e+01 ; 3.740353e+02 ];
omc_error_11 = [ 4.229111e-03 ; 5.988719e-03 ; 9.431074e-03 ];
Tc_error_11  = [ 5.185358e+00 ; 2.027550e+00 ; 4.241329e+00 ];

%-- Image #12:
omc_12 = [ -2.012200e+00 ; -1.136399e+00 ; 1.211894e+00 ];
Tc_12  = [ 8.146191e+00 ; -5.413630e+01 ; 4.614107e+02 ];
omc_error_12 = [ 1.091272e-02 ; 6.030493e-03 ; 1.116440e-02 ];
Tc_error_12  = [ 6.535094e+00 ; 2.487324e+00 ; 4.771861e+00 ];

%-- Image #13:
omc_13 = [ -1.314007e+00 ; -2.439110e+00 ; 2.192520e-01 ];
Tc_13  = [ -5.013597e+01 ; -8.559703e+01 ; 3.802425e+02 ];
omc_error_13 = [ 4.286740e-03 ; 7.070316e-03 ; 8.781909e-03 ];
Tc_error_13  = [ 5.229712e+00 ; 2.066251e+00 ; 4.343618e+00 ];

%-- Image #14:
omc_14 = [ -1.434443e+00 ; -1.987004e+00 ; 5.200962e-01 ];
Tc_14  = [ -1.633406e+01 ; -1.074390e+02 ; 3.869853e+02 ];
omc_error_14 = [ 7.063172e-03 ; 8.694686e-03 ; 8.724082e-03 ];
Tc_error_14  = [ 5.440391e+00 ; 2.093092e+00 ; 4.398483e+00 ];

%-- Image #15:
omc_15 = [ -2.563123e+00 ; -7.999827e-01 ; 1.000061e+00 ];
Tc_15  = [ -3.504277e+01 ; 3.072509e+01 ; 4.459699e+02 ];
omc_error_15 = [ 9.505390e-03 ; 3.415069e-03 ; 1.335908e-02 ];
Tc_error_15  = [ 6.202695e+00 ; 2.401382e+00 ; 4.943912e+00 ];

%-- Image #16:
omc_16 = [ -1.776083e+00 ; -1.375252e+00 ; 1.019751e+00 ];
Tc_16  = [ 9.301506e+00 ; -6.525076e+01 ; 4.733156e+02 ];
omc_error_16 = [ 1.008420e-02 ; 7.546526e-03 ; 9.973300e-03 ];
Tc_error_16  = [ 6.722185e+00 ; 2.554801e+00 ; 5.020713e+00 ];

%-- Image #17:
omc_17 = [ -1.530698e+00 ; -1.631043e+00 ; 8.345891e-01 ];
Tc_17  = [ 2.044327e+01 ; -7.380770e+01 ; 4.295551e+02 ];
omc_error_17 = [ 8.869081e-03 ; 8.630456e-03 ; 8.881376e-03 ];
Tc_error_17  = [ 6.146885e+00 ; 2.322568e+00 ; 4.656603e+00 ];

%-- Image #18:
omc_18 = [ -1.233498e+00 ; -2.384286e+00 ; 4.424354e-01 ];
Tc_18  = [ 2.550191e+01 ; -8.898509e+01 ; 3.764776e+02 ];
omc_error_18 = [ 5.642875e-03 ; 7.563031e-03 ; 9.216243e-03 ];
Tc_error_18  = [ 5.411981e+00 ; 2.040959e+00 ; 4.222926e+00 ];

%-- Image #19:
omc_19 = [ -2.574055e+00 ; -1.212096e+00 ; 7.417598e-01 ];
Tc_19  = [ -7.266068e+01 ; 3.158974e+01 ; 4.144555e+02 ];
omc_error_19 = [ 7.890513e-03 ; 2.939872e-03 ; 1.235605e-02 ];
Tc_error_19  = [ 5.628199e+00 ; 2.240651e+00 ; 4.795145e+00 ];

%-- Image #20:
omc_20 = [ -1.653117e+00 ; -2.168565e+00 ; 4.049853e-01 ];
Tc_20  = [ -7.037379e+01 ; -7.492209e+01 ; 3.393722e+02 ];
omc_error_20 = [ 5.897135e-03 ; 6.392834e-03 ; 9.069985e-03 ];
Tc_error_20  = [ 4.577880e+00 ; 1.851967e+00 ; 3.942823e+00 ];

%-- Image #21:
omc_21 = [ -2.087762e+00 ; -1.853447e+00 ; 5.402917e-01 ];
Tc_21  = [ -9.585383e+01 ; -2.639307e+01 ; 3.528950e+02 ];
omc_error_21 = [ 6.896622e-03 ; 4.882212e-03 ; 1.028249e-02 ];
Tc_error_21  = [ 4.682328e+00 ; 1.935044e+00 ; 4.117948e+00 ];

%-- Image #22:
omc_22 = [ -2.608259e+00 ; -1.043292e+00 ; 9.746667e-01 ];
Tc_22  = [ -9.439824e+01 ; -2.772586e+01 ; 4.401244e+02 ];
omc_error_22 = [ 9.309741e-03 ; 2.791390e-03 ; 1.378589e-02 ];
Tc_error_22  = [ 5.941195e+00 ; 2.399982e+00 ; 4.951370e+00 ];

