#!/usr/bin/env python

import rospy

if __name__ == "__main__":

    rospy.init_node('test_parameter', anonymous=True)

    if rospy.has_param('~name') and rospy.has_param('~words'):
        name = rospy.get_param('~name')
        words = rospy.get_param('~words')
        rospy.loginfo('%s said, "%s"', name, words)
    else:
        rospy.loginfo('Please provide the parameters!')