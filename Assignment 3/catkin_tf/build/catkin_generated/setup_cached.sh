#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/hanyuz/catkin_tf/devel:$CMAKE_PREFIX_PATH"
export PWD="/home/hanyuz/catkin_tf/build"
export ROSLISP_PACKAGE_DIRECTORIES="/home/hanyuz/catkin_tf/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/hanyuz/catkin_tf/src:$ROS_PACKAGE_PATH"