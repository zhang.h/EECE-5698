# encoding: utf-8
# python 3

# for python 2 compatibility
from __future__ import print_function
import sys
import serial
import lcm
from imulcm import imu_lcm
from class_imu import imu_data

class imu_serial:
    def __init__(self, port_name):
        # Initialization
        print('IMU Device on Serial Port {}...'.format(port_name))
        try:
            self.port = serial.Serial(port_name, 115200, timeout=1.0)
        except:
            print('Serial port couldn\'t open! Exit.')
            sys.exit()

        # Check the data
        print('Serial port initializing...')
        errorCount = 0
        while True:
            serial_data = self.port.readline().decode()
            if serial_data.startswith('$VNYMR'):
                print('Initialization finished!')
                break
            if errorCount > 20:
                print('Initialization failed! Exit!')
                sys.exit()
            else:
                errorCount += 1

    def show_data(self):
        # Print received data
        while True:
            serial_data = self.port.readline().decode()
            if serial_data.startswith('$VNYMR'):
                imu_received = imu_data(serial_data)
                imu_received.print_data()
                break

    # pack up LCM message
    def pack_lcm_msg(self, imu_data_class):
        imu_lcm_msg = imu_lcm()
        imu_lcm_msg.yaw = imu_data_class.yaw
        imu_lcm_msg.pitch = imu_data_class.pitch
        imu_lcm_msg.roll = imu_data_class.roll
        imu_lcm_msg.mag_x = imu_data_class.mag_x
        imu_lcm_msg.mag_y = imu_data_class.mag_y
        imu_lcm_msg.mag_z = imu_data_class.mag_z
        imu_lcm_msg.accel_x = imu_data_class.accel_x
        imu_lcm_msg.accel_y = imu_data_class.accel_y
        imu_lcm_msg.accel_z = imu_data_class.accel_z
        imu_lcm_msg.gyro_x = imu_data_class.gyro_x
        imu_lcm_msg.gyro_y = imu_data_class.gyro_y
        imu_lcm_msg.gyro_z = imu_data_class.gyro_z
        imu_lcm_msg.vnymr = imu_data_class.original_str
        return imu_lcm_msg

    def send_lcm_msg(self, count):
        # read the current data from IMU and send via LCM
        # use "count" to set the number of the LCM message to send
        # to keep sending message, set "count = 0"
        if count == 0:
            nonstop_flag = True
        else:
            nonstop_flag = False
        print('Sending IMU data...')
        imu_lcm_send = lcm.LCM()
        while True:
            if count == 0 and nonstop_flag == False:
                break
            serial_data = self.port.readline().decode()
            if serial_data.startswith('$VNYMR'):
                imu_lcm_msg = self.pack_lcm_msg(imu_data(serial_data))
                imu_lcm_send.publish('IMUData', imu_lcm_msg.encode())
                print('IMU data sent!')
                count -= 1

    def __exit__(self):
        self.port.close()

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('Usage: {} <serial_port>'.format(sys.argv[0]))
        sys.exit(0)
    my_imu = imu_serial(sys.argv[1])
    my_imu.show_data()
    my_imu.send_lcm_msg(0)

