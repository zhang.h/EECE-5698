# encoding: utf-8
# python 3
# Based on NMEA

# for python 2 compatibility
from __future__ import print_function
import utm

class gps_data:
    # Extract data from GPS receiver
    def __init__(self, gps_serial_data):
        gpgga = gps_serial_data.split(',')
        self.original_str = gps_serial_data
        self.time = gpgga[1]
        self.latitude = float(gpgga[2])/100
        self.NS = gpgga[3]
        self.longitude = float(gpgga[4])/100
        self.SW = gpgga[5]
        self.altitude = float(gpgga[9]) # unit: meter

        # Calculate the correct latitude and longitude
        self.latitude = int(self.latitude) + (float(gpgga[2]) - int(self.latitude)*100) /60.0
        if self.NS == 'S':
            self.latitude = -self.latitude
        self.longitude = int(self.longitude) + (float(gpgga[4]) - int(self.longitude)*100) /60.0
        if self.SW == 'W':
            self.longitude = -self.longitude

        # Convert to UTM coordinates
        utm_temp = utm.from_latlon(self.latitude, self.longitude)
        self.utm_x = utm_temp[0]
        self.utm_y = utm_temp[1]
        self.utm_zonenumber = utm_temp[2]
        self.utm_zoneletter = utm_temp[3]

    def print_data(self):
        print('GPS Time: {} UTC'.format(self.time))
        print('Latitude: {0} degrees {1}'.format(self.latitude, self.NS))
        print('Longitude: {0} degrees {1}'.format(self.longitude, self.SW))
        print('Altitude: {} m'.format(self.altitude))
