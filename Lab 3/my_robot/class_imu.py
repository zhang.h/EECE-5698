# encoding: utf-8
# python 3

# for python 2 compatibility
from __future__ import print_function

class imu_data:
    # Extract data from IMU
    def __init__(self, imu_serial_data):
        imudata = imu_serial_data.split(',')
        self.original_str = imu_serial_data
        self.yaw = float(imudata[1]) # deg
        self.pitch = float(imudata[2])  # deg
        self.roll = float(imudata[3])  # deg
        self.mag_x = float(imudata[4])  # Gauss
        self.mag_y = float(imudata[5])  # Gauss
        self.mag_z = float(imudata[6])  # Gauss
        self.accel_x = float(imudata[7])  # m/s2
        self.accel_y = float(imudata[8])  # m/s2
        self.accel_z = float(imudata[9])  # m/s2
        self.gyro_x = float(imudata[10])  # rad/s
        self.gyro_y = float(imudata[11])  # rad/s
        last_string = imudata[12]
        last_string_end = last_string.index("*")
        self.gyro_z = float(last_string[0:last_string_end])  # rad/s


    def print_data(self):
        # print('Raw Data: {}'.format(self.original_str))
        print('Attitude:',end=' ')
        print('Yaw: {0} degrees, Pitch: {1} degrees, Roll: {2} degrees'.format(self.yaw, self.pitch ,self.roll))
        print('Magnetic:',end=' ')
        print('X: {0} Gauss, Y: {1} Gauss, Z: {2} Gauss'.format(self.mag_x, self.mag_y, self.mag_z))
        print('Acceleration:', end=' ')
        print('X: {0} m/s^2, Y: {1} m/s^2, Z: {2} m/s^2'.format(self.accel_x, self.accel_y, self.accel_z))
        print('Angular:', end=' ')
        print('X: {0} rad/s, Y: {1} rad/s, Z: {2} rad/s'.format(self.gyro_x, self.gyro_y, self.gyro_z))
