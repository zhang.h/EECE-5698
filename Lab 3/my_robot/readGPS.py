# encoding: utf-8
# python 3
# Based on NMEA

# for python 2 compatibility
from __future__ import print_function
import sys
import serial
import lcm
from gpslcm import gps_lcm
from class_gps import gps_data

class gps_serial:
    def __init__(self, port_name):
        # Initialization
        print('GPS Device on Serial Port {}...'.format(port_name))
        try:
            self.port = serial.Serial(port_name, 4800, timeout=1.0)
        except:
            print('Serial port couldn\'t open! Exit.')
            sys.exit()

        # Check the data
        print('Serial port initializing...')
        errorCount = 0
        while True:
            serial_data = self.port.readline().decode()
            if serial_data.startswith('$GPGGA'):
                print('Initialization finished!')
                break
            if errorCount > 20:
                print('Initialization failed! Exit!')
                sys.exit()
            else:
                errorCount += 1

    def show_data(self):
        # Print GPS position
        print('Make sure strong GPS signal!')
        while True:
            serial_data = self.port.readline().decode()
            if serial_data.startswith('$GPGGA'):
                temp_data = serial_data.split(',')
                if int(temp_data[6]) == 0:
                    print('Weak GPS signal!')
                    continue
                else:
                    gps_received = gps_data(serial_data)
                    gps_received.print_data()
                    break

    # pack up LCM message
    def pack_lcm_msg(self, gps_data_class):
        gps_lcm_msg = gps_lcm()
        gps_lcm_msg.time = gps_data_class.time
        gps_lcm_msg.latitude = gps_data_class.latitude
        gps_lcm_msg.longitude = gps_data_class.longitude
        gps_lcm_msg.altitude = gps_data_class.altitude
        gps_lcm_msg.utm_x = gps_data_class.utm_x
        gps_lcm_msg.utm_y = gps_data_class.utm_y
        gps_lcm_msg.utm_zonenumber = gps_data_class.utm_zonenumber
        gps_lcm_msg.utm_zoneletter = gps_data_class.utm_zoneletter
        gps_lcm_msg.gpgga = gps_data_class.original_str
        return gps_lcm_msg

    def send_lcm_msg(self, count):
        # read the current position from GPS receiver and send via LCM
        # use "count" to set the number of the LCM message to send
        # to keep sending message, set "count = 0"
        if count == 0:
            nonstop_flag = True
        else:
            nonstop_flag = False
        print('Sending GPS data...')
        gps_lcm_send = lcm.LCM()
        while True:
            if count == 0 and nonstop_flag == False:
                break
            serial_data = self.port.readline().decode()
            if serial_data.startswith('$GPGGA'):
                temp_data = serial_data.split(',')
                if int(temp_data[6]) == 0:
                    print('Weak GPS signal!')
                    continue
                else:
                    gps_lcm_msg = self.pack_lcm_msg(gps_data(serial_data))
                    gps_lcm_send.publish('GPSData', gps_lcm_msg.encode())
                    print('GPS data sent!')
                    count -= 1
                    continue

    def __exit__(self):
        self.port.close()

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('Usage: {} <serial_port>'.format(sys.argv[0]))
        sys.exit(0)
    my_gps = gps_serial(sys.argv[1])
    my_gps.show_data()
    my_gps.send_lcm_msg(0)
