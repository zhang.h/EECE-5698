% Stationary data analysis (Please run import_stationary_data.m first!)

mx_offset = mean(mag_x);
my_offset = mean(mag_y);
mz_offset = mean(mag_z);

figure
subplot(1,3,1);
plot(utime_imu, mag_x);
hold on;
line([0 utime_imu(end)], [mx_offset mx_offset],'Color','red');
legend('mag_x', strcat('mean(mag_x): ', num2str(mx_offset), 'Gauss'));
xlabel('Time (s)')
ylabel('Magnetic (Gauss)')
title('x-axis magnetic');
subplot(1,3,2);
plot(utime_imu, mag_y);
hold on;
line([0 utime_imu(end)], [my_offset my_offset],'Color','red');
legend('mag_y', strcat('mean(mag_y): ', num2str(my_offset), 'Gauss'));
xlabel('Time (s)')
ylabel('Magnetic (Gauss)')
title('y-axis magnetic');
subplot(1,3,3);
plot(utime_imu, mag_z);
hold on;
line([0 utime_imu(end)], [mz_offset mz_offset],'Color','red');
legend('mag_z', strcat('mean(mag_z): ', num2str(mz_offset), 'Gauss'));
xlabel('Time (s)')
ylabel('Magnetic (Gauss)')
title('z-axis magnetic');