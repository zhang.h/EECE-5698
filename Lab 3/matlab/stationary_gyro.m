% Stationary data analysis (Please run import_stationary_data.m first!)

gx_offset = mean(gyro_x);
gy_offset = mean(gyro_y);
gz_offset = mean(gyro_z);

figure

subplot(1,3,1);
plot(utime_imu, gyro_x);
hold on;
line([0 utime_imu(end)], [gx_offset gx_offset],'Color','red');
legend('gyro_x', strcat('mean(gyro_x): ', num2str(gx_offset), 'rad/s'));
xlabel('Time (s)')
ylabel('Angular Rate (rad/s)')
title('x-axis angular rate');
subplot(1,3,2);
plot(utime_imu, gyro_y);
hold on;
line([0 utime_imu(end)], [gy_offset gy_offset],'Color','red');
legend('gyro_y', strcat('mean(gyro_y): ', num2str(gy_offset), 'rad/s'));
xlabel('Time (s)')
ylabel('Angular Rate (rad/s)')
title('y-axis angular rate');
subplot(1,3,3);
plot(utime_imu, gyro_z);
hold on;
line([0 utime_imu(end)], [gz_offset gz_offset],'Color','red');
legend('gyro_z', strcat('mean(gyro_z): ', num2str(gz_offset), 'rad/s'));
xlabel('Time (s)')
ylabel('Angular Rate (rad/s)')
title('z-axis angular rate');