% Integrate to get the path (xc = 0)
%% Load data
data_file = 'loop_data2.mat';
load(data_file, 'utime_imu');
load(data_file, 'acc_x');
load(data_file, 'acc_y');
load(data_file, 'gyro_z');
utime_imu = utime_imu - utime_imu(1);
utime_imu = double(utime_imu)/1e+6;
%% Integral
%apply low-pass filter to gyro_z
fs = 40;
fc_w = 0.2;
[b_w, a_w] = butter(2, fc_w/(fs/2));
gyro_z_f = filter(b_w, a_w, gyro_z);
%apply low-pass filter to the acc_x
fc_acc = 0.02;
[b_acc, a_acc] = butter(2, fc_acc/(fs/2));
acc_y_f = filter(b_acc, a_acc, acc_y);
%integrate to get vx
v_x = cumtrapz(utime_imu, acc_x);
%dot-product to get w.*vx
w_v = v_x .* gyro_z_f;
%% Plot
figure
subplot(1,3,1)
plot(utime_imu, acc_y_f);
title('y-axis acceleration');
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
subplot(1,3,2)
plot(utime_imu, w_v);
title('centripetal acceleration ({\omega} * v_x)');
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
subplot(1,3,3)
plot(utime_imu, acc_y_f - w_v);
title('difference (Acc_y - {\omega} * v_x)');
xlabel('Time (s)')
ylabel('{\Delta}Acceleration (m/s^2)')