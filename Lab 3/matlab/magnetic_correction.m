% Magnetometer calibration (Please run import_data.m first!)
%% Load data
data_file = 'loop_data2.mat';
load(data_file, 'utime_imu');
load(data_file, 'yaw');
load(data_file, 'mag_x');
load(data_file, 'mag_y');
utime_imu = utime_imu - utime_imu(1);
utime_imu = double(utime_imu)/1e+6;
%% Magnetometer calibration
%Hard-iron
mag_x_offset = 0.5*(max(mag_x) + min(mag_x));
mag_y_offset = 0.5*(max(mag_y) + min(mag_y));
mag_x_c = mag_x - mag_x_offset;
mag_y_c = mag_y - mag_y_offset;
%Soft-iron
%get the major and the minor axis of the ellipse
mag_ellipse = fit_ellipse(mag_x_c, mag_y_c);
%rotate to align the ellipse's major axe with x axe
rotate_angle = -mag_ellipse.phi;
rotation = [cos(rotate_angle), sin(rotate_angle); -sin(rotate_angle), cos(rotate_angle)];
mag_c = rotation * cat(1, mag_x_c, mag_y_c);
mag_x_c = mag_c(1,:);
mag_y_c = mag_c(2,:);
%compute the scale factor
sigma = mag_ellipse.short_axis/mag_ellipse.long_axis;
mag_x_c = mag_x_c * sigma;
%rotate back
rotation_back = [cos(-rotate_angle), sin(-rotate_angle); -sin(-rotate_angle), cos(-rotate_angle)];
mag_c = rotation_back * cat(1, mag_x_c, mag_y_c);
mag_x_c = mag_c(1,:);
mag_y_c = mag_c(2,:);
%% Get magnetometer headings
%low-pass filter
[mag_rad, ~] = cart2pol(-mag_y_c, mag_x_c);
mag_rad = unwrap(mag_rad);
fs = 40;
fc_m = 0.2;
[b_m, a_m] = butter(2, fc_m/(fs/2));
mag_rad_f = filter(b_m, a_m, mag_rad);
%% Reference: yaw angle
yaw_rad = -deg2rad(yaw);
yaw_rad = unwrap(yaw_rad);
%% Plot
figure
subplot(1,2,1)
scatter(mag_x, mag_y, 5, 'filled');
grid on;
axis equal;
title('magnetometer raw data');
xlabel('Mag_x (Gauss)')
ylabel('Mag_y (Gauss)')

subplot(1,2,2)
scatter(mag_x_c, mag_y_c, 5, 'filled');
grid on;
axis equal;
title('soft-iron and hard-iron effects eliminated');
xlabel('Mag_x (Gauss)')
ylabel('Mag_y (Gauss)')

figure
plot(utime_imu, yaw_rad, 'r');
hold on
plot(utime_imu, mag_rad, 'g');
plot(utime_imu, mag_rad_f, 'b');
title('yaw angle comparison');
xlabel('Time (s)')
ylabel('(Unwrapped) Yaw Angle (rad)')
legend('yaw angle output', 'yaw angle from magnetometer', 'yaw angle from magnetometer (Butterworth LPF applied, f_c=0.2Hz)');