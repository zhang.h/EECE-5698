% Load all data (stationary data)
%% Extract data from LCM logs
%Import Java class
javaaddpath('my_types.jar');
javaaddpath('lcm.jar');
%Load LCM log file
log_file = lcm.logging.Log('stationary', 'r');
%Read log file and interpret into array
count_imu = 1;
utime_imu = int64(zeros(1, 1000000));
yaw = double(zeros(1, 1000000));
mag_x = double(zeros(1, 1000000));
mag_y = double(zeros(1, 1000000));
mag_z = double(zeros(1, 1000000));
acc_x = double(zeros(1, 1000000));
acc_y = double(zeros(1, 1000000));
acc_z = double(zeros(1, 1000000));
gyro_x = double(zeros(1, 1000000));
gyro_y = double(zeros(1, 1000000));
gyro_z = double(zeros(1, 1000000));
while true
    try
        ev = log_file.readNext();
        
        if strcmp(ev.channel, 'IMUData')
            imuinfo = imulcm.imu_lcm(ev.data);
            if imuinfo.mag_x ~= 0
                yaw(count_imu) = imuinfo.yaw;
                mag_x(count_imu) = imuinfo.mag_x;
                mag_y(count_imu) = imuinfo.mag_y;
                mag_z(count_imu) = imuinfo.mag_z;
                acc_x(count_imu) = imuinfo.accel_x;
                acc_y(count_imu) = imuinfo.accel_y;
                acc_z(count_imu) = imuinfo.accel_z;
                gyro_x(count_imu) = imuinfo.gyro_x;
                gyro_y(count_imu) = imuinfo.gyro_y;
                gyro_z(count_imu) = imuinfo.gyro_z;
                utime_imu(count_imu) = ev.utime;
                count_imu = count_imu + 1;
            end
        end
        
    catch err
        break;
    end
end
%remove blank data
utime_imu = utime_imu(1:count_imu-1);
yaw = yaw(1:count_imu-1);
mag_x = mag_x(1:count_imu-1);
mag_y = mag_y(1:count_imu-1);
mag_z = mag_z(1:count_imu-1);
acc_x = acc_x(1:count_imu-1);
acc_y = acc_y(1:count_imu-1);
acc_z = acc_z(1:count_imu-1);
gyro_x = gyro_x(1:count_imu-1);
gyro_y = gyro_y(1:count_imu-1);
gyro_z = gyro_z(1:count_imu-1);
%time starts with zero, unit: second
utime_imu = utime_imu - utime_imu(1);
utime_imu = double(utime_imu)/1e+6;
%% Remove Java class
javarmpath('my_types.jar');
javarmpath('lcm.jar');