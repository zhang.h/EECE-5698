% Plot the trajectory using magnetics heading and estimate xc
%% Load data
data_file = 'loop_data2.mat';
load(data_file, 'utime_imu');
load(data_file, 'yaw');
load(data_file, 'mag_x');
load(data_file, 'mag_y');
load(data_file, 'acc_x');
load(data_file, 'acc_y');
load(data_file, 'gyro_z');
offset_ax = -0.248;
offset_ay = 0.016;
acc_x = acc_x - offset_ax;
acc_y = acc_y - offset_ay;
utime_imu = utime_imu - utime_imu(1);
utime_imu = double(utime_imu)/1e+6;
%% Magnetometer calibration
%Hard-iron
mag_x_offset = 0.5*(max(mag_x) + min(mag_x));
mag_y_offset = 0.5*(max(mag_y) + min(mag_y));
mag_x_c = mag_x - mag_x_offset;
mag_y_c = mag_y - mag_y_offset;
%Soft-iron
%get the major and the minor axis of the ellipse
mag_ellipse = fit_ellipse(mag_x_c, mag_y_c);
%rotate to align the ellipse's major axis with x axis
rotate_angle = -mag_ellipse.phi;
rotation = [cos(rotate_angle), sin(rotate_angle); -sin(rotate_angle), cos(rotate_angle)];
mag_c = rotation * cat(1, mag_x_c, mag_y_c);
mag_x_c = mag_c(1,:);
mag_y_c = mag_c(2,:);
%compute the scale factor
sigma = mag_ellipse.short_axis/mag_ellipse.long_axis;
mag_x_c = mag_x_c * sigma;
%rotate back
rotation_back = [cos(-rotate_angle), sin(-rotate_angle); -sin(-rotate_angle), cos(-rotate_angle)];
mag_c = rotation_back * cat(1, mag_x_c, mag_y_c);
mag_x_c = mag_c(1,:);
mag_y_c = mag_c(2,:);
%% Integral
%apply low-pass filter to gyro_z 
fs = 40;
fc_w = 0.2;
[b_w, a_w] = butter(2, fc_w/(fs/2));
gyro_z_f = filter(b_w, a_w, gyro_z);
%get magnetic heading and apply low-pass filter
[mag_rad, ~] = cart2pol(-mag_y_c, mag_x_c);
mag_rad_f = unwrap(mag_rad);
fc_m = 0.2;
[b_m, a_m] = butter(2, fc_m/(fs/2));
mag_rad_f = filter(b_m, a_m, mag_rad_f);
%use magnetic headings
theta = mag_rad_f;
%apply low-pass filter to the acc_x
fc_acc = 0.02;
[b_acc, a_acc] = butter(2, fc_acc/(fs/2));
acc_x = filter(b_acc, a_acc, acc_x);
acc_y = filter(b_acc, a_acc, acc_y);
%integrate to get vx
v_x = cumtrapz(utime_imu, acc_x);
%dot-product to get w.*vx
w_v = v_x .* gyro_z_f;
%integrate to get the path
s = cumtrapz(utime_imu, v_x);
s_delta = diff(s);
s_delta = cat(2, 0, s_delta);
s_x_delta = s_delta .* cos(theta);
s_y_delta = s_delta .* sin(theta);
s_x = cumsum(s_x_delta);
s_y = cumsum(s_y_delta);
%estimate xc
temp_1 = acc_y - w_v;
temp_2 = gyro_z_f .* cumtrapz(utime_imu , gyro_z_f .* gyro_z_f) + cat(2,0,diff(gyro_z_f));
xc_estimate = temp_1 ./ temp_2;
xc_estimate(1) = 0;
xc_estimate = filter(b_acc, a_acc, xc_estimate);
xc = mean(xc_estimate)
%% Plot the path
figure
plot(s_x, s_y);
color_mag = linspace(1, 10, length(s_x));
scatter(s_x, s_y, 2, color_mag, 'filled');
axis equal
xlabel('X (m)')
ylabel('Y (m)')