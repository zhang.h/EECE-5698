% Integrate to get the yaw
%% Load data
data_file = 'loop_data2.mat';
load(data_file, 'utime_imu');
load(data_file, 'yaw');
load(data_file, 'gyro_z');
utime_imu = utime_imu - utime_imu(1);
utime_imu = double(utime_imu)/1e+6;
%% Integral
%get yaw value
yaw_rad = -deg2rad(yaw);
yaw_rad = unwrap(yaw_rad);
%apply low-pass filter to gyro_z and integrate gyro_z to get
fs = 40;
fc_w = 0.2;
[b_w, a_w] = butter(2, fc_w/(fs/2));
gyro_z_f = filter(b_w, a_w, gyro_z);
yaw2_rad_f = cumtrapz(utime_imu, -gyro_z_f);
%not apply low-pass filter
yaw2_rad = cumtrapz(utime_imu, -gyro_z);
yaw2_rad = unwrap(yaw2_rad);
%% Plot
figure
plot(utime_imu, yaw_rad, 'r');
hold on
plot(utime_imu, yaw2_rad, 'g');
plot(utime_imu, yaw2_rad_f, 'b');
title('yaw angle comparison');
xlabel('Time (s)')
ylabel('(Unwrapped) Yaw Angle (rad)')
legend('yaw angle output', 'yaw angle from Gyro_z', 'yaw angle from Gyro_z (Butterworth LPF applied, f_c=0.2Hz)'...
    ,'Location','northwest');