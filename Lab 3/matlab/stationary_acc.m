% Stationary data analysis (Please run import_stationary_data.m first!)

ax_offset = mean(acc_x);
ay_offset = mean(acc_y);
az_offset = mean(acc_z);

figure
subplot(1,3,1);
plot(utime_imu, acc_x);
hold on;
line([0 utime_imu(end)], [ax_offset ax_offset],'Color','red');
legend('acc_x', strcat('mean(acc_x): ', num2str(ax_offset), 'm/s^2'));
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
title('x-axis acceleration');
subplot(1,3,2);
plot(utime_imu, acc_y);
hold on;
line([0 utime_imu(end)], [ay_offset ay_offset],'Color','red');
legend('acc_y', strcat('mean(acc_y): ', num2str(ay_offset), 'm/s^2'));
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
title('y-axis acceleration');
subplot(1,3,3);
plot(utime_imu, acc_z);
hold on;
line([0 utime_imu(end)], [az_offset az_offset],'Color','red');
legend('acc_z', strcat('mean(acc_z): ', num2str(az_offset), 'm/s^2'));
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
title('z-axis acceleration');