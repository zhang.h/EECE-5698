% Integrate to get the yaw
%% Load data
data_file = 'loop_data2.mat';
load(data_file, 'utm_x');
load(data_file, 'utm_y');
load(data_file, 'lat');
load(data_file, 'long');

%% Plot the path
utm_x_min = min(utm_x);
utm_y_min = min(utm_y);
utm_x = utm_x - utm_x_min;
utm_y = utm_y - utm_y_min;
figure
plot(utm_x, utm_y);
xlabel(strcat('UTM X (+', num2str(utm_x_min), ') (m)'))
ylabel(strcat('UTM Y (+', num2str(utm_y_min), ') (m)'))
axis equal
%% Plot the path on the map
position = geoshape(lat,long);
webmap('openstreetmap')
wmline(position)