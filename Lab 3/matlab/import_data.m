% Load all data and save them to the .mat file
%% Extract data from LCM logs
%Import Java class
javaaddpath('my_types.jar');
javaaddpath('lcm.jar');
%Load LCM log file
log_file = lcm.logging.Log('lcmlog-2018-02-26.01', 'r');
%Read log file and interpret into array
count = 1;
count_imu = 1;
utime_gps = int64(zeros(1, 1000000));
utm_x = double(zeros(1, 1000000));
utm_y = double(zeros(1, 1000000));
lat = double(zeros(1, 1000000));
long = double(zeros(1, 1000000));
alt = double(zeros(1, 1000000));
utime_imu = int64(zeros(1, 1000000));
yaw = double(zeros(1, 1000000));
mag_x = double(zeros(1, 1000000));
mag_y = double(zeros(1, 1000000));
acc_x = double(zeros(1, 1000000));
acc_y = double(zeros(1, 1000000));
gyro_z = double(zeros(1, 1000000));
while true
    try
        ev = log_file.readNext();
        
        if strcmp(ev.channel, 'GPSData')
            gpsinfo = gpslcm.gps_lcm(ev.data);
            if gpsinfo.utm_x ~= 0
                utm_x(count) = gpsinfo.utm_x;
                utm_y(count) = gpsinfo.utm_y;
                lat(count) = gpsinfo.latitude;
                long(count) = gpsinfo.longitude;
                alt(count) = gpsinfo.altitude;
                utime_gps(count) = ev.utime;
                count = count + 1;
            end
        end
        
        if strcmp(ev.channel, 'IMUData')
            imuinfo = imulcm.imu_lcm(ev.data);
            if imuinfo.mag_x ~= 0
                yaw(count_imu) = imuinfo.yaw;
                mag_x(count_imu) = imuinfo.mag_x;
                mag_y(count_imu) = imuinfo.mag_y;
                acc_x(count_imu) = imuinfo.accel_x;
                acc_y(count_imu) = imuinfo.accel_y;
                gyro_z(count_imu) = imuinfo.gyro_z;
                utime_imu(count_imu) = ev.utime;
                count_imu = count_imu + 1;
            end
        end
        
    catch err
        break;
    end
end
%remove blank data
utime_gps = utime_gps(1:count-1);
utm_x = utm_x(1:count-1);
utm_y = utm_y(1:count-1);
lat = lat(1:count-1);
long = long(1:count-1);
alt = alt(1:count-1);
utime_imu = utime_imu(1:count_imu-1);
yaw = yaw(1:count_imu-1);
mag_x = mag_x(1:count_imu-1);
mag_y = mag_y(1:count_imu-1);
acc_x = acc_x(1:count_imu-1);
acc_y = acc_y(1:count_imu-1);
gyro_z = gyro_z(1:count_imu-1);
%% Remove Java class
javarmpath('my_types.jar');
javarmpath('lcm.jar');
%% Remove Java class
save('loop_data2.mat','utime_gps','utm_x','utm_y','lat','long','alt','utime_imu','yaw','mag_x','mag_y','acc_x','acc_y','gyro_z');