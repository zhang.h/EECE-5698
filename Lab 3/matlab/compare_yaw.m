% Yaw angle comparison
%% Load data
data_file = 'loop_data2.mat';
load(data_file, 'utime_imu');
load(data_file, 'yaw');
load(data_file, 'mag_x');
load(data_file, 'mag_y');
load(data_file, 'gyro_z');
utime_imu = utime_imu - utime_imu(1);
utime_imu = double(utime_imu)/1e+6;
%% Magnetometer calibration
%Hard-iron
mag_x_offset = 0.5*(max(mag_x) + min(mag_x));
mag_y_offset = 0.5*(max(mag_y) + min(mag_y));
mag_x_c = mag_x - mag_x_offset;
mag_y_c = mag_y - mag_y_offset;
%Soft-iron
%get the major and the minor axis of the ellipse
mag_ellipse = fit_ellipse(mag_x_c, mag_y_c);
%rotate to align the ellipse's major axe with x axe
rotate_angle = -mag_ellipse.phi;
rotation = [cos(rotate_angle), sin(rotate_angle); -sin(rotate_angle), cos(rotate_angle)];
mag_c = rotation * cat(1, mag_x_c, mag_y_c);
mag_x_c = mag_c(1,:);
mag_y_c = mag_c(2,:);
%compute the scale factor
sigma = mag_ellipse.short_axis/mag_ellipse.long_axis;
mag_x_c = mag_x_c * sigma;
%rotate back
rotation_back = [cos(-rotate_angle), sin(-rotate_angle); -sin(-rotate_angle), cos(-rotate_angle)];
mag_c = rotation_back * cat(1, mag_x_c, mag_y_c);
mag_x_c = mag_c(1,:);
mag_y_c = mag_c(2,:);
%% Get magnetometer headings
%low-pass filter
[mag_rad, ~] = cart2pol(-mag_y_c, mag_x_c);
mag_rad = unwrap(mag_rad);
fs = 40;
fc_m = 0.2;
[b_m, a_m] = butter(2, fc_m/(fs/2));
mag_rad_f = filter(b_m, a_m, mag_rad);
%% Reference: yaw angle
yaw_rad = -deg2rad(yaw);
yaw_rad = unwrap(yaw_rad);
%% Integral
%apply high-pass filter to gyro_z
fs = 40;
fc_w = 0.2;
[b_w, a_w] = butter(2, fc_w/(fs/2), 'high');
gyro_z_f = filter(b_w, a_w, gyro_z);
%not apply low-pass filter and integrate gyro_z to get
yaw2_rad = cumtrapz(utime_imu, -gyro_z);
yaw2_rad = unwrap(yaw2_rad);
%% Plot
figure
plot(utime_imu, yaw_rad, 'r');
hold on
plot(utime_imu, mag_rad_f, 'g');
plot(utime_imu, yaw2_rad, 'b');
title('yaw angle comparison');
xlabel('Time (s)')
ylabel('(Unwrapped) Yaw Angle (rad)')
legend('yaw angle output', 'yaw angle from megnetometer', 'yaw angle from Gyro_z'...
    ,'Location','northwest');
%% Complimentary filter
theta = 0.8*(gyro_z_f * 0.025 + yaw2_rad) + 0.2*mag_rad_f;
figure
plot(utime_imu, yaw_rad, 'r');
hold on
plot(utime_imu, theta, 'b');
title('yaw angle comparison (complimentary filter)');
xlabel('Time (s)')
ylabel('(Unwrapped) Yaw Angle (rad)')
legend('yaw angle output', 'yaw angle from megnetometer and Gyro_z','Location','northwest');