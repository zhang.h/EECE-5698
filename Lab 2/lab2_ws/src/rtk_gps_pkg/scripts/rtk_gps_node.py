#!/usr/bin/env python

import rospy
import sys
import socket
from sensor_msgs.msg import NavSatFix

# Extract $GPGGA string
def nmea_string(string):
    try:
        start = string.index("$GPGGA,")
    except:
        return "0"

    try:
        end = string.index("\r\n", start)
    except:
        return "0"

    return string[start:end]

# $GPGGA string handler
def get_position(gps_gpgga):
    gpgga = gps_gpgga.split(',')
    if gpgga[6] == '0':
        return 0
    latitude = float(gpgga[2]) / 100
    NS = gpgga[3]
    longitude = float(gpgga[4]) / 100
    SW = gpgga[5]
    altitude = float(gpgga[9])  # unit: meter

    # compute the latitude/longitude/altitude
    latitude = int(latitude) + (float(gpgga[2]) - int(latitude) * 100) / 60.0
    if NS == 'S':
        latitude = -latitude
    longitude = int(longitude) + (float(gpgga[4]) - int(longitude) * 100) / 60.0
    if SW == 'W':
        longitude = -longitude

    return latitude, longitude, altitude

# Receive data over TCP
def gps_receive(ip, port):
    while True:
        s = socket.socket()
        s.connect((ip, port))
        gpsdata = s.recv(1024)
        gpgga = nmea_string(gpsdata)
        if gpgga != "0":
            position = get_position(gpgga)
            if position == 0:
                s.close()
                continue
            s.close()
            return position
        else:
            s.close()


def talker(ip, port):
    pub = rospy.Publisher('rtk_fix', NavSatFix, queue_size=10)
    rate = rospy.Rate(1) # GPS data refresh rate: 1hz
    gpsmsg = NavSatFix()

    while not rospy.is_shutdown():
        position = gps_receive(ip, port)
        gpsmsg.latitude = position[0]
        gpsmsg.longitude = position[1]
        gpsmsg.altitude = position[2]
        rospy.loginfo(gpsmsg)
        pub.publish(gpsmsg)
        rate.sleep()


if __name__ == "__main__":

    rospy.init_node('rtk_gps_node', anonymous=True)
    ip = rospy.get_param('~host')
    port = rospy.get_param('~port')

    try:
        talker(ip, port)
    except rospy.ROSInterruptException:
        pass
