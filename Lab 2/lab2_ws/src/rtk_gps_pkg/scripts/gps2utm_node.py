#!/usr/bin/env python

import rospy
import utm
from sensor_msgs.msg import NavSatFix
from nav_msgs.msg import Odometry

def talker(x, y, z):
    pub = rospy.Publisher('utm_fix', Odometry, queue_size=10)
    utmmsg = Odometry()
    utmmsg.pose.pose.position.x = x
    utmmsg.pose.pose.position.y = y
    utmmsg.pose.pose.position.z = z

    rospy.loginfo(utmmsg)
    pub.publish(utmmsg)

def callback(data):
    rospy.loginfo("Lat: %f, Lon: %f, Alt: %f" % (data.latitude, data.longitude, data.altitude))
    utm_x = utm.from_latlon(data.latitude, data.longitude)[0]
    utm_y = utm.from_latlon(data.latitude, data.longitude)[1]
    talker(utm_x, utm_y, data.altitude)

def listener():

    rospy.init_node('gps2utm_node', anonymous=True)

    rospy.Subscriber('rtk_fix', NavSatFix, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
