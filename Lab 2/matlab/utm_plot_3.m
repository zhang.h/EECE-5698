%Load rosbag file
bag_obj = rosbag('2018-02-08-16-24-16.bag');
%Extract utm_x, utm_y, z
odom_all = readMessages(select(bag_obj, 'Topic', '/utm_fix'));
count = size(odom_all, 1);
utm = zeros(count, 3);
for i = 1:count
    utm(i,:) = [odom_all{i}.Pose.Pose.Position.X ...
        odom_all{i}.Pose.Pose.Position.Y odom_all{i}.Pose.Pose.Position.Z];
end
utm_x_min = min(utm(:,1));
utm_y_min = min(utm(:,2));
utm(:,1) = utm(:,1) - utm_x_min;
utm(:,2) = utm(:,2) - utm_y_min;
%Plot
figure
c = linspace(1, 10, length(utm(:,1)));
scatter(utm(:,1), utm(:,2), [], c, 'filled')
axis equal
xlabel(strcat('UTM X (+', num2str(utm_x_min), ') (m)'))
ylabel(strcat('UTM Y (+', num2str(utm_y_min), ') (m)'))
xlim([0 0.012])
ylim([0 0.012])
hold on
[~, central,sumd]=kmeans(utm(:,1:2),1);
plot(central(1), central(2), 'rx', 'LineWidth', 1.5, 'MarkerSize', 10);

figure
z_hist=histogram(utm(:,3));
xlabel('Altitude (m)')
ylabel('Counts')
