%Load rosbag file
bag_obj = rosbag('2018-02-08-16-47-41.bag');
%Extract utm_x, utm_y, z
odom_all = readMessages(select(bag_obj, 'Topic', '/utm_fix'));
fix_all = readMessages(select(bag_obj, 'Topic', '/rtk_fix'));
count = size(odom_all, 1);
utm = zeros(count, 3);
fix = zeros(count, 3);
for i = 1:count
    utm(i,:) = [odom_all{i}.Pose.Pose.Position.X ...
        odom_all{i}.Pose.Pose.Position.Y odom_all{i}.Pose.Pose.Position.Z];
    fix(i,:) = [fix_all{i}.Latitude fix_all{i}.Longitude fix_all{i}.Altitude];
end
utm_x_min = min(utm(:,1));
utm_y_min = min(utm(:,2));
utm(:,1) = utm(:,1) - utm_x_min;
utm(:,2) = utm(:,2) - utm_y_min;
%Plot
figure
c = linspace(1, 10, length(utm(:,1)));
scatter(utm(:,1), utm(:,2), [], c, 'filled')
axis equal
xlabel(strcat('UTM X (+', num2str(utm_x_min), ') (m)'))
ylabel(strcat('UTM Y (+', num2str(utm_y_min), ') (m)'))
xlim([-0.5 inf])
ylim([-0.5 inf])
hold on
l1 = polyfit(cat(1,utm(1:5,1),utm(20:end,1)), cat(1,utm(1:5,2),utm(20:end,2)), 1);
fplot(@(x) l1(1)*x+l1(2),[-0.1 2.9],'--')
l2 = polyfit(utm(6:10,1), utm(6:10,2), 1);
fplot(@(x) l2(1)*x+l2(2),[2.8 4.6],'--')
l3 = polyfit(utm(10:16,1), utm(10:16,2), 1);
fplot(@(x) l3(1)*x+l3(2),[1.4 4.5],'--')
l4 = polyfit(utm(16:20,1), utm(16:20,2), 1);
fplot(@(x) l4(1)*x+l4(2),[-0.1 1.7],'--')

position = geoshape(fix(:,1),fix(:,2));
webmap('openstreetmap')
wmline(position)