%Load rosbag file
bag_obj = rosbag('2018-02-08-14-14-37.bag');
%Extract utm_x, utm_y, z
odom_all = readMessages(select(bag_obj, 'Topic', '/utm_fix'));
fix_all = readMessages(select(bag_obj, 'Topic', '/rtk_fix'));
count = size(odom_all, 1);
utm = zeros(count, 3);
fix = zeros(count, 3);
for i = 1:count
    utm(i,:) = [odom_all{i}.Pose.Pose.Position.X ...
        odom_all{i}.Pose.Pose.Position.Y odom_all{i}.Pose.Pose.Position.Z];
    fix(i,:) = [fix_all{i}.Latitude fix_all{i}.Longitude fix_all{i}.Altitude];
end
utm = utm(1:35,:);
fix = fix(1:35,:);
utm_x_min = min(utm(:,1));
utm_y_min = min(utm(:,2));
utm(:,1) = utm(:,1) - utm_x_min;
utm(:,2) = utm(:,2) - utm_y_min;
%Plot
figure
c = linspace(1, 10, length(utm(:,1)));
scatter(utm(:,1), utm(:,2), [], c, 'filled')
axis equal
xlabel(strcat('UTM X (+', num2str(utm_x_min), ') (m)'))
ylabel(strcat('UTM Y (+', num2str(utm_y_min), ') (m)'))
xlim([-0.2 6])
ylim([-0.2 6])
hold on
[l1, s1] = polyfit(cat(1,utm(1:9,1),utm(32:35,1)), cat(1,utm(1:9,2),utm(32:35,2)), 1);
fplot(@(x) l1(1)*x+l1(2),[1 6],'--')
l2 = polyfit(utm(11:15,1), utm(11:15,2), 1);
fplot(@(x) l2(1)*x+l2(2),[0 1.7],'--')
l3 = polyfit(utm(16:23,1), utm(16:23,2), 1);
fplot(@(x) l3(1)*x+l3(2),[0 5],'--')
l4 = polyfit(utm(26:31,1), utm(26:31,2), 1);
fplot(@(x) l4(1)*x+l4(2),[4.5 5.7],'--')

position = geoshape(fix(:,1),fix(:,2));
webmap('openstreetmap')
wmline(position)